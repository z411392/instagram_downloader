import 'dart:async';
import 'dart:typed_data';

import 'package:instagram_downloader/instagram_downloader.dart';
import 'package:puppeteer/puppeteer.dart';
import 'dart:convert' show JsonUtf8Encoder, jsonDecode, utf8;
import 'dart:io' show Directory, File, sleep;
import 'package:logging/logging.dart' show Logger, Level, hierarchicalLoggingEnabled;
import "package:path/path.dart" show dirname, join;
import 'dart:io' show Platform;

final directory = Directory(join(dirname(Platform.script.path), "images/"));
const timelineApi = 'https://www.instagram.com/api/v1/feed/timeline/';
final logger = Logger('instagram_downloader')
  ..level = Level.INFO
  ..onRecord.listen(
    (record) {
      print(record.message);
    },
  );
void main(List<String> arguments) async {
  hierarchicalLoggingEnabled = true;
  if (!await directory.exists()) await directory.create(recursive: true);
  logger.info('正在開啟瀏覽器');
  final browser = await puppeteer.launch(devTools: false, headless: false, args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-gpu']);
  final page = (await browser.pages)[0];
  logger.info('正在設定 cookies');
  final cookieFile = File(join(dirname(Platform.script.path), 'cookies.json'));
  final cookies = jsonDecode(await cookieFile.readAsString())
      .entries
      .map((MapEntry<String, dynamic> entry) => CookieParam(name: entry.key, value: entry.value, domain: '.instagram.com'))
      .cast<CookieParam>()
      .toList();
  await page.setCookies(cookies);
  logger.info('正在前往 instagram 頁面');
  page.goto('https://www.instagram.com', wait: Until.domContentLoaded);
  final xpath = "//*[@role='dialog']//button[contains(text(), 'Not Now')]";
  final completer = Completer<void>();
  page.waitForXPath(xpath).then((_) => page.$x(xpath).then((buttons) => buttons[0].click().then((_) => completer.complete())));
  await completer.future;
  final records = ((page) async* {
    while (true) {
      List<String> imageUrls = (await page.evaluate<List<dynamic>>(
              '() => Array.from(document.querySelectorAll("article")).filter((article) => article.querySelector("div>img")?.src).map((article) => article.querySelector("div>img").src)'))
          .cast<String>();
      final records = Stream.fromFutures(imageUrls.map((imageUrl) {
        logger.info('正在解析圖片 $imageUrl');
        return convertToDownloadEntry(imageUrl);
      }).toList());
      await for (final record in records) {
        if (record == null) continue;
        yield record;
      }
      bool hasRequested = false;
      final responsed = page.waitForResponse(timelineApi);
      page.waitForRequest(timelineApi).then((_) => hasRequested = true);
      while (!hasRequested) {
        await page.evaluate('scrollTo(0, document.body.scrollHeight)');
        sleep(Duration(milliseconds: 33));
      }
      await responsed;
      sleep(Duration(milliseconds: 200));
    }
  })(page);
  logger.info('正在前往抓取圖片網址清單');
  int count = 0;
  await for (final (String filename, Uint8List bytes) in records) {
    final file = File('${directory.path}$filename');
    if (await file.exists()) continue;
    logger.info('正在寫入檔案 ${file.path}');
    await file.writeAsBytes(bytes);
    count += 1;
    if (count >= 20) break;
  }
  final json = utf8.decode(JsonUtf8Encoder('  ').convert({for (final cookieParam in await page.cookies()) cookieParam.name: cookieParam.value}));
  await cookieFile.writeAsString(json);
  await browser.close();
}
