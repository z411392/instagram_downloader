part of instagram_downloader;

final dio = Dio();

Future<(String, Uint8List)?> convertToDownloadEntry(String imageUrl) async {
  String? extension;
  if (imageUrl.contains('.jpg') || imageUrl.contains('.jpeg')) extension = 'jpeg';
  if (imageUrl.contains('.png')) extension = 'png';
  if (extension == null) return null;
  final bytes = (await dio.get<Uint8List>(imageUrl, options: Options(responseType: ResponseType.bytes))).data;
  if (bytes == null) return null;
  final image = decodeImage(bytes);
  if (image == null) return null;
  final filename = '${phash(image)}.$extension';
  return (filename, bytes);
}
